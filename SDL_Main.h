#ifndef SDL_MAIN_H
#define SDL_MAIN_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "TetrisGrid.h"

class SDL_Main
{
public:
    enum DelayTime { DELAY_ROTATE=15, DELAY_MOVELR=10 };
    enum BlockSize {BLOCK_SIZE=30};
    enum Status { END, PLAYING, ANIMATING, PAUSE };
    enum Corner {CORNER=1};
    SDL_Main(int w=640, int h=480, int bpp=32, unsigned fps=30);
    ~SDL_Main();
    bool init(const char* = NULL);
    bool load();
    bool mainLoop();
private:
    void refresh();
    void showShape(const TetrisShape& shape);
    void showGrid(int rows);
    void drawColoredBlock(int r, int c, Uint32 color);
    void handleKeyPressing();
    void handleKeyPressed();
    void drawScoreLevel();
    void darkenScreen(const char* reason, const char* hint);
private:
    //WINDOW ATTRIBUTES
    int scr_width;
    int scr_height;
    int scr_bpp;
    unsigned fps;
    //SURFACE POINTERS
    SDL_Surface* screen;
    SDL_Surface* scoreLabel;
    SDL_Surface* levelLabel;
    //FONT POINTERS
    TTF_Font* arial24;
    //MAIN LOOP VARIABLES
    bool endMain;
    SDL_Event event;
    //ADDITIONAL VARIABLES
    TetrisGame game;
    Uint8 zDelay;
    Uint8 xDelay;
    Uint8 leftDelay;
    Uint8 rightDelay;
    bool drawScrLv;
};

#endif // SDL_MAIN_H
