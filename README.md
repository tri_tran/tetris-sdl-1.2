# README #

This is an old project (Dec 2013): Tetris game using SDL 1.2

### Screenshots ###

![play](screenshots/play.png)
![score](screenshots/score.png)
![pause](screenshots/pause.png)
![gameover](screenshots/gameover.png)


### Setup ###

* SDL 1.2
* SDL_gfx (to draw rounded rectangles)