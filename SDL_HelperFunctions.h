#ifndef SDL_HELPERFUNCTIONS_H
#define SDL_HELPERFUNCTIONS_H

#include "SDL/SDL_image.h"

SDL_Surface *loadImage(const char* filename, int r=-1, int g=-1, int b=-1);
SDL_Surface *loadImageAlpha(const char* filename);
void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination,
                  SDL_Rect* clip = NULL);
void makeLighterDarker(Uint8 r,Uint8 g,Uint8 b,double lum,
                       Uint8& out_r,Uint8& out_g,Uint8& out_b);
void makeLighterDarker(const SDL_Color& org, SDL_Color& dst, double lum);
Uint32 toRGBA(Uint32 argb);
Uint32 makeRGBA(Uint8 r, Uint8 g, Uint8 b, Uint8 a);

#endif // SDL_HELPERFUNCTIONS_H
