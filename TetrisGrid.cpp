#include "TetrisGrid.h"
#include <algorithm>

//-----------------------------------------------
TetrisGame::TetrisGame()
: grid(ROWS, NULL), shape(), nextShape(),
  score(0), baseLevel(0), animatedFrame(0),
  status(PLAYING)
{
    for (int r=0; r<ROWS; ++r)
        grid[r] = new int[COLS];

    for (int r=0; r<ROWS; ++r)
        for (int c=0; c<COLS; ++c)
            grid[r][c] = 0;

    shape.setPosition(COLS/2,0);
    nextShape.setPosition(COLS + 4, 3);

    updateScoreLevel();
}
//-----------------------------------------------
TetrisGame::~TetrisGame()
{
    for (int r=0; r<ROWS; ++r)
        delete [] grid[r];
}
//-----------------------------------------------
bool TetrisGame::move(int d, int n)
{
    bool canMove = true;
    int ox = shape.x();
    int oy = shape.y();
    while (n-- && canMove)
    {
        shape.setPosition(shape.x()+(d==LEFT?-1:1), shape.y());
        for (int i=0; i<shape.size(); ++i)
        {
            int r = shape.y()+shape[i].y;
            int c = shape.x()+shape[i].x;
            if (c < 0 || c > COLS-1) // left/right boundaries
            {
                canMove = false;
                break;
            }
            else if (r>=0 && grid[r][c]) // collisions
            {
                canMove = false;
                break;
            }
        }
    }
    if (!canMove) shape.setPosition(ox, oy);
    return canMove;
}
//-----------------------------------------------
void TetrisGame::rotateShape(bool isClockWised)
{
    bool canRotate = true;
    int moveDirection = -1;
    int moveSteps = 0;
    shape.rotate(isClockWised);
    for (int i =0; i<shape.size(); ++i)
    {
        int r = shape.y()+shape[i].y;
        int c = shape.x()+shape[i].x;
        if (r >= ROWS) // bottom bound
        {
            canRotate = false;
            break;
        }
        else if (c < 0)  // left bound
        {
            moveDirection = RIGHT;
            if (-c > moveSteps)
                moveSteps = -c;
        }
        else if (c > COLS-1)  // right bound
        {
            moveDirection = LEFT;
            if (c-COLS+1 > moveSteps)
                moveSteps = c-COLS+1;
        }
        else if (r>=0 && grid[r][c]) //collisions
        {
            canRotate = false;
            break;
        }
    }
    if (!canRotate || moveDirection!=-1)
        shape.rotate(!isClockWised);

    if (moveDirection!=-1)
        if (move(moveDirection,moveSteps))
            shape.rotate(isClockWised);
}
//-----------------------------------------------
bool TetrisGame::dropDown()
{
    bool canDrop = true;
    shape.setPosition(shape.x(),shape.y()+1);
    for (int i =0; i<shape.size(); ++i)
    {
        int sx = shape.x()+shape[i].x;
        int sy = shape.y()+shape[i].y;
        if (sy > ROWS-1) // bottom boundary
        {
            canDrop = false;
            break;
        }
        if (grid[sy][sx])  // collisions
        {
            canDrop = false;
            break;
        }
    }
    if (!canDrop)
    {
        shape.setPosition(shape.x(),shape.y()-1);
        if (addShapeToGrid())
        {
            checkFullLines();
            shape = nextShape;
            shape.setPosition(COLS/2,0);
            nextShape.randomize();
            if (!inGoodPosition()) status = END;
        }
        else status = END;
    }
    return canDrop;
}
//-----------------------------------------------
bool TetrisGame::addShapeToGrid()
{
    bool valid = true;
    for (int i =0; i<shape.size(); ++i)
    {
        int r = shape.y() + shape[i].y;
        int c = shape.x() + shape[i].x;
        if (r<0) valid = false;
        if (r>=0) grid[r][c] = shape.getColor();
    }
    return valid;
}
//-----------------------------------------------
bool TetrisGame::checkFullLines()
{
    fullRows.clear();
    for (int i=0; i<shape.size(); ++i)
    {
        int r = shape.y() + shape[i].y;
        if (std::find(fullRows.begin(), fullRows.end(), r)==fullRows.end())
        {
            bool full = true;
            for (int c=0; c<COLS; ++c)
            {
                if (!grid[r][c])
                {
                    full = false;
                    break;
                }
            }
            if (full) fullRows.push_back(r);
        }
    }
    if (fullRows.size() > 1)
        std::sort(fullRows.begin(), fullRows.end());

    if (!fullRows.empty())
    {
        updateScoreLevel();
        status = ANIMATING;
        animatedFrame = COLS;
    }
    return !fullRows.empty();
}
//-----------------------------------------------
bool TetrisGame::inGoodPosition()
{
    bool good = true;
    for (int i=0; i<shape.size(); ++i)
    {
        int r = shape.y() + shape[i].y;
        int c = shape.x() + shape[i].x;
        if (r>=0 && grid[r][c])
        {
            good = false;
            break;
        }
    }
    return good;
}
//-----------------------------------------------
void TetrisGame::updateScoreLevel()
{
    score += fullRows.size()*(fullRows.size()+4);
    level = (baseLevel + score/100)%MAX_LEVEL + 1;
}

void TetrisGame::collapsing()
{
    int r = fullRows.back();

    int* tempRow = grid[r];
    for (int i=r; i>0; --i)
        grid[i] = grid[i-1];
    grid[0] = tempRow;

    fullRows.pop_back();
    for (size_t i=0; i<fullRows.size(); ++i)
         ++fullRows[i];
}
//-----------------------------------------------
void TetrisGame::animation()
{
    if (animatedFrame > 0)
    {
        for (size_t i=0; i<fullRows.size(); ++i)
        {
            int r = fullRows[i];
            int c = i%2? animatedFrame-1 : COLS-animatedFrame;
            grid[r][c] = 0;
        }
    }
    else if (!fullRows.empty())
    {
        if (animatedFrame%3==0)
            collapsing();
    }
    else
    {
        status = PLAYING;
    }
}
//-----------------------------------------------
void TetrisGame::reset()
{
    for (int r=0; r<ROWS; ++r)
        for (int c=0; c<COLS; ++c)
            grid[r][c] = 0;

    score = 0;
    baseLevel = 0;

    shape.randomize();
    shape.setPosition(COLS/2,0);
    nextShape.randomize();

    updateScoreLevel();

    status = PLAYING;
}
//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
