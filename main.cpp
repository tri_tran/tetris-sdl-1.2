#include "SDL_Main.h"
#include <cstdlib>
#include <ctime>

int main(int argc, char* argv[])
{
    srand(time(0));
    SDL_Main mymain(640,640,32,60);

    if (!mymain.init("Tetris")) return 1;
    if (!mymain.load()) return 1;
    if (!mymain.mainLoop()) return 1;

    return 0;
}
