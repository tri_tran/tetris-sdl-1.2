#include "TetrisShape.h"
#include <cstdlib>

const std::string TetrisShape::Shapes = "OIJLTSZ";

TetrisShape::TetrisShape()
: px(0), py(0), color(0), shape(Shapes[rand()%Shapes.length()])
{
    init();
    int rot = rand()%4;
    while (rot--) rotate(0);
}

TetrisShape::TetrisShape(char c)
: px(0), py(0), color(0), shape(c)
{
    init();
}

void TetrisShape::init()
{
    data.push_back(Point( 0, 0));
    switch (shape)
    {
    case 'O':
        data.push_back(Point( 1, 0));
        data.push_back(Point( 0, 1));
        data.push_back(Point( 1, 1));
        color = 0xffff00ff; //YELLOW
        break;
    case 'I':
        data.push_back(Point(-1, 0));
        data.push_back(Point( 1, 0));
        data.push_back(Point( 2, 0));
        color = 0x00ffffff; //CYAN
        break;
    case 'L':
        data.push_back(Point(-1, 0));
        data.push_back(Point( 1, 0));
        data.push_back(Point( 1,-1));
        color = 0xffa500ff; //ORANGE
        break;
    case 'J':
        data.push_back(Point(-1, 0));
        data.push_back(Point( 1, 0));
        data.push_back(Point(-1,-1));
        color = 0x0000ffff; //BLUE
        break;
    case 'T':
        data.push_back(Point(-1, 0));
        data.push_back(Point( 1, 0));
        data.push_back(Point( 0,-1));
        color = 0xff00ffff; //MAGENTA
        break;
    case 'Z':
        data.push_back(Point( 1, 0));
        data.push_back(Point(-1,-1));
        data.push_back(Point( 0,-1));
        color = 0xff0000ff; //RED
        break;
    case 'S':
        data.push_back(Point(-1, 0));
        data.push_back(Point( 0,-1));
        data.push_back(Point( 1,-1));
        color = 0x00ff00ff; //GREEN
        break;
    default:
        color = 0x000000ff; //BLACK
        break;
    }
}

TetrisShape::~TetrisShape()
{
}

void TetrisShape::rotate(bool isClockWised)
{
    for (unsigned i=0; i<data.size(); ++i)
    {
        int temp = data[i].x;
        if (isClockWised)
        {
            data[i].x = -data[i].y + (shape=='I'||shape=='O');
            data[i].y = temp;
        }
        else
        {
            data[i].x = data[i].y;
            data[i].y = -temp + (shape=='I'||shape=='O');
        }
    }
}

void TetrisShape::randomize()
{
    shape = Shapes[rand()%Shapes.length()];
    data.clear();
    init();
    int rot = rand()%4;
    while (rot--) rotate(0);
}
