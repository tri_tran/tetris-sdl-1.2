#include "SDL_Main.h"
#include "SDL_HelperFunctions.h"
#include "SDL/SDL_gfxPrimitives.h"
#include <sstream>

//-----------------------------------------------
SDL_Main::SDL_Main(int w, int h, int b, unsigned f)
: scr_width(w), scr_height(h), scr_bpp(b), fps(f),
  screen(NULL), scoreLabel(NULL), levelLabel(NULL),
  arial24(NULL),
  endMain(false),
  game(),
  zDelay(0), xDelay(0), leftDelay(0), rightDelay(0),
  drawScrLv(false)
{
}
//-----------------------------------------------
SDL_Main::~SDL_Main()
{
    //Free surfaces
    SDL_FreeSurface(screen);
    SDL_FreeSurface(scoreLabel);
    SDL_FreeSurface(levelLabel);
    //Free fonts
    TTF_CloseFont(arial24);

    TTF_Quit();
    SDL_Quit();
}
//-----------------------------------------------
bool SDL_Main::init(const char* title)
{
    if (!SDL_Init(SDL_INIT_EVERYTHING)==-1) return false;

    SDL_Surface* windowIcon = SDL_LoadBMP("icon.bmp");
    if (!windowIcon) return false;
    Uint32 colorkey = SDL_MapRGB(windowIcon->format,255,255,255);
    SDL_SetColorKey(windowIcon, SDL_SRCCOLORKEY, colorkey);
    SDL_WM_SetIcon(windowIcon, NULL);
    SDL_FreeSurface(windowIcon);

    screen = SDL_SetVideoMode(scr_width, scr_height, scr_bpp, SDL_SWSURFACE);
    if (!screen) return false;
    if(TTF_Init() == -1) return false;
    if (title) SDL_WM_SetCaption(title, NULL);

    return true;
}
//-----------------------------------------------
bool SDL_Main::load()
{
    arial24 = TTF_OpenFont("arial.ttf", 24);
    if (!arial24) return false;

    drawScoreLevel();

    return true;
}
//-----------------------------------------------
bool SDL_Main::mainLoop()
{
    int speed = fps - 2*game.getLevel() + 2;
    unsigned frame = 0;

    while (!endMain)
    {
        unsigned startLt = SDL_GetTicks(); //start loop time
        if (++frame == fps) frame = 0;

        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                endMain = true;
            if (event.type == SDL_KEYDOWN)
                handleKeyPressed();
        }

        if (game.getStatus() == PLAYING)
        {
            handleKeyPressing();
            if (!--speed) //drop shape
            {
                speed = fps - (fps/TetrisGame::MAX_LEVEL)*game.getLevel() +
                    (fps/TetrisGame::MAX_LEVEL);
                game.dropDown();
                if (game.getStatus()==END)
                    darkenScreen("Game Over", "Press N to play new game");
            }
        }

        if (game.getStatus() == ANIMATING)
        {
            game.animation();
            if (frame%2==0) game.decreaseAnimatedFrame();
        }

        refresh();

        if (SDL_Flip(screen)==-1) return false;

        //delay frame if needed
        if (SDL_GetTicks()-startLt < 1000/fps)
            SDL_Delay(1000/fps - (SDL_GetTicks()-startLt));
    }

    return true;
}
//-----------------------------------------------
void SDL_Main::drawColoredBlock(int r, int c, Uint32 color)
{
    int x = c * BLOCK_SIZE;
    int y = r * BLOCK_SIZE;
    roundedBoxColor(screen, x+1, y+1, x+BLOCK_SIZE-1, y+BLOCK_SIZE-1,
                    CORNER, color);
    roundedRectangleColor(screen, x+1, y+1, x+BLOCK_SIZE-1, y+BLOCK_SIZE-1,
                          CORNER, 0x000000ff);
}
//-----------------------------------------------
void SDL_Main::refresh()
{
    if (game.getStatus() == END || game.getStatus() == PAUSE) return;

    if (game.getStatus() == ANIMATING)
    {
        if (drawScrLv) drawScoreLevel();
        drawScrLv = false;
    }
    else drawScrLv = true;

    SDL_FillRect( screen, &screen->clip_rect,
                  SDL_MapRGB(screen->format, 0x00, 0xcc, 0xff) );

    //show grid
    showGrid(TetrisGame::ROWS);

    //show shape
    if (game.getStatus() != ANIMATING) showShape(game.getShape());
    showShape(game.getNextShape());

    //show score and level
    applySurface(BLOCK_SIZE*(TetrisGame::COLS+2), 200, scoreLabel, screen);
    applySurface(BLOCK_SIZE*(TetrisGame::COLS+2), 260, levelLabel, screen);
}
//-----------------------------------------------
void SDL_Main::handleKeyPressed()
{
    if (game.getStatus() == PLAYING)
    {
        switch (event.key.keysym.sym)
        {
        case SDLK_z:
            game.rotateShape(0);
            zDelay = DELAY_ROTATE;
            break;
        case SDLK_x:
            game.rotateShape(1);
            xDelay = DELAY_ROTATE;
            break;
        case SDLK_LEFT:
        case SDLK_KP1:
            game.move(TetrisGame::LEFT);
            leftDelay = DELAY_MOVELR;
            break;
        case SDLK_RIGHT:
        case SDLK_KP3:
            game.move(TetrisGame::RIGHT);
            rightDelay = DELAY_MOVELR;
            break;
        case SDLK_a:
            game.increaseSpeed();
            game.updateScoreLevel();
            drawScoreLevel();
            break;
        case SDLK_s:
            game.decreaseSpeed();
            game.updateScoreLevel();
            drawScoreLevel();
            break;
        case SDLK_p:
            game.pause();
            darkenScreen("Game paused", "Press P to resume game");
            break;
        default:
            break;
        }
    }
    else if (game.getStatus() == PAUSE)
    {
        if (event.key.keysym.sym == SDLK_p)
            game.unPause();
    }
    else if (game.getStatus() == END)
    {
        if (event.key.keysym.sym == SDLK_n)
        {
            game.reset();
            drawScoreLevel();
        }
    }
}
//-----------------------------------------------
void SDL_Main::handleKeyPressing()
{
    Uint8* keyStates = SDL_GetKeyState(NULL);
    if (keyStates[SDLK_z])
    {
        if (zDelay) --zDelay;
        else game.rotateShape(0);
    }
    if (keyStates[SDLK_x])
    {
        if (xDelay) --xDelay;
        else game.rotateShape(1);
    }
    if (keyStates[SDLK_LEFT] || keyStates[SDLK_KP1])
    {
        if (leftDelay) --leftDelay;
        else game.move(TetrisGame::LEFT);
    }
    if (keyStates[SDLK_RIGHT] || keyStates[SDLK_KP3])
    {
        if (rightDelay) --rightDelay;
        else game.move(TetrisGame::RIGHT);
    }
    if (keyStates[SDLK_DOWN] || keyStates[SDLK_KP2])
    {
        game.dropDown();
        darkenScreen("Game Over", "Press N to play new game");
    }
}
//-----------------------------------------------
void SDL_Main::drawScoreLevel()
{
    SDL_Color black = {0,0,0};

    std::ostringstream lvlss;
    lvlss << "Level: " << game.getLevel();
    if (levelLabel) SDL_FreeSurface(levelLabel);
    levelLabel = TTF_RenderText_Solid(arial24, lvlss.str().c_str(), black);

    std::ostringstream scrss;
    scrss << "Score: " << game.getScore();
    if (scoreLabel) SDL_FreeSurface(scoreLabel);
    scoreLabel = TTF_RenderText_Solid(arial24, scrss.str().c_str(), black);
}
//-----------------------------------------------
void SDL_Main::showShape(const TetrisShape& ts)
{
    for (int i=0; i<ts.size(); ++i)
    {
        int r = ts.y() + ts[i].y;
        int c = ts.x() + ts[i].x;
        drawColoredBlock(r, c, ts.getColor());
    }
}
//-----------------------------------------------
void SDL_Main::showGrid(int rows)
{
    if (rows<0) rows=0;
    else if (rows>TetrisGame::ROWS) rows=TetrisGame::ROWS;
    for (int r=0; r<rows; ++r)
        for (int c=0; c<TetrisGame::COLS; ++c)
            drawColoredBlock(r, c, game.at(r,c)?game.at(r,c):0x3388aaff);
}
//-----------------------------------------------
void SDL_Main::darkenScreen(const char* reason, const char* hint)
{
    if (game.getStatus() == END) showGrid(4);

    boxColor(screen,0,0,scr_width,scr_height, 0x00000088);

    SDL_Color orange = {255,255,0};
    TTF_Font* arial72 = TTF_OpenFont("arial.ttf", 72);
    if (!arial72) return;

    SDL_Surface* reasonMessage = TTF_RenderText_Solid(arial72,reason,orange);
    SDL_Surface* hintMessage = TTF_RenderText_Solid(arial24,hint,orange);
    applySurface((scr_width - reasonMessage->clip_rect.w)/2,
                 (scr_height - reasonMessage->clip_rect.h)/2,
                 reasonMessage, screen);
    applySurface((scr_width - hintMessage->clip_rect.w)/2,
                 (scr_height - reasonMessage->clip_rect.h)/2 +
                  reasonMessage->clip_rect.h,
                 hintMessage, screen);
    SDL_FreeSurface(reasonMessage);
    SDL_FreeSurface(hintMessage);
    TTF_CloseFont(arial72);
}
