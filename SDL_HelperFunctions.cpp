#include "SDL_HelperFunctions.h"
#include <fstream>

//original by lazyfoo at http://lazyfoo.net
SDL_Surface *loadImage(const char* filename, int r, int g, int b)
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load(filename);

    if (loadedImage)
    {
        optimizedImage = SDL_DisplayFormat(loadedImage);
        SDL_FreeSurface(loadedImage);
        if (optimizedImage && r!=-1 && g!=-1 && b!=-1)
        {
            Uint32 colorkey = SDL_MapRGB(optimizedImage->format, r, g, b);
            SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, colorkey);
        }
    }

    return optimizedImage;
}

SDL_Surface *loadImageAlpha(const char* filename)
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load(filename);

    if (loadedImage)
    {
        optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
        SDL_FreeSurface(loadedImage);
    }

    return optimizedImage;
}

//original by lazyfoo at http://lazyfoo.net
void applySurface(int x, int y, SDL_Surface* src, SDL_Surface* dst, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface(src, clip, dst, &offset);
}

void makeLighterDarker(Uint8 r,Uint8 g,Uint8 b,double lum,
                       Uint8& out_r,Uint8& out_g,Uint8& out_b)
{
    if (lum>1.0)
    {
        out_r = r + (255-r)*(lum-1);
        out_g = g + (255-g)*(lum-1);
        out_b = b + (255-b)*(lum-1);
    }
    else
    {
        out_r = r*lum;
        out_g = g*lum;
        out_b = b*lum;
    }
}

void makeLighterDarker(const SDL_Color& org, SDL_Color& dst, double lum)
{
    makeLighterDarker(org.r, org.g, org.b, lum, dst.r, dst.g, dst.b);
}

Uint32 toRGBA(Uint32 argb)
{
    Uint8 a = (argb>>24);
    if (!a) a = 0xff;
    return (argb<<8) + a;
}

Uint32 makeRGBA(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    return (r<<24) + (g<<16) + (b<<8) + a;
}
